Test framework for testing getting Articles from https://www.healthcare.gov/ 

Tests started with following Maven command:
mvn clean verify 
or via Pipeline

Results appears in the Artifact section or in the Target folder:
target/site/serenity/index.html
